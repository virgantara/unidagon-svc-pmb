'use strict';
module.exports = function(app) {
  var todoList = require('../controller/appController');

  app.route('/pmb/m/prodi/diterima/get')
    .get(todoList.getMabaDiterima);

  app.route('/pmb/m/prodi/pendaftar/get')
    .get(todoList.getMabaPendaftar);

  app.route('/pmb/m/prodi/get')
    .get(todoList.getMabaPerprodi);

  app.route('/pmb/oppal/tagihan/cek')
    .post(todoList.cekVa);

  app.route('/pmb/oppal/tagihan/pay')
    .post(todoList.oppal_pmb_pay);

  app.route('/ujian/jawaban')
    .get(todoList.getJawaban);

  app.route('/ujian/jawaban/sync')
    .post(todoList.syncJawaban);

  app.route('/ujian/soal/jenis/prodi')
    .get(todoList.getJenisSoalProdi);

  app.route('/ujian/soal/jenis')
    .get(todoList.getSoalByJenis);

  app.route('/pmb/rekap/camaba')
    .get(todoList.getRekapCamaba);

  app.route('/pmb/rekap/perhari')
    .get(todoList.getCamabaPerhari);

  
};