'use strict';

var Pmb = require('../model/appModel.js');
var Oppal = require('../model/oppalModel.js');
const { validationResult } = require('express-validator')

var response = require('../../res.js');

exports.getMabaDiterima = function(req,res){
  Pmb.getMabaDiterima(req.query, function(err,values){
    if(err) res.send(err)

    response.ok(values,res)
  })
}

exports.getMabaPendaftar = function(req,res){
  Pmb.getMabaPendaftar(req.query, function(err,values){
    if(err) res.send(err)

    response.ok(values,res)
  })
}

exports.getMabaPerprodi = function(req,res){
  Pmb.getMabaPerprodi(req.query, function(err,values){
    if(err) res.send(err)

    response.ok(values,res)
  })
}

exports.cekVa = function(req, res) {

  const errors = validationResult(req)
  if (!errors.isEmpty()) {
      response.badRequest('Bad Request', res);
  }

  Oppal.cekVa(req.body, function(err, values) {
    
    if (err)
      response.badRequest(values, res);

    response.ok(values, res);

  });
};

exports.oppal_pmb_pay = function(req, res) {

  console.log('Incoming transaction...')
  console.log(req.body.TransactionID)
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
      console.log('Oops, bad request')
      console.log(req.body)
      response.badRequest('Bad Request', res);
  }

  Oppal.oppal_pay(req.body, function(err, values) {
    
    if (err){
      console.log('Oops, something wrong')
      console.log(err)
      response.badRequest(values, res);
    }
    else{
      response.ok(values, res);  
    }

    

  });
};


exports.getCamabaPerhari = function(req,res){
  Pmb.getCamabaPerhari(function(err,values){
    if(err) res.send(err)

    response.ok(values,res)
  })
}

exports.getRekapCamaba = function(req,res){
  Pmb.getRekapCamaba(function(err,values){
    if(err) res.send(err)

    response.ok(values,res)
  })
}

exports.getJawaban = function(req,res){
  Pmb.getJawaban(req.query.qid, req.query.pid, function(err,values){
    if(err) res.send(err)

    response.ok(values,res)
  })
}

exports.syncJawaban = function(req,res){
  Pmb.syncJawaban(req.body,function(err,values){
    if(err) res.send(err)

    response.ok(values,res)
  })
}

exports.getJenisSoalProdi = function(req, res){
  Pmb.getJenisSoalProdi(
    req.query.id1,
    req.query.id2,
    req.query.id3, function(err, values){
    if(err) res.send(err)

    if(values){
      let promises = values.map(function(item){
        return new Promise((resolve,reject)=>{
          let par = new Object
          par.id = item.jpid
          par.quiz_id = req.query.quiz_id
          Pmb.getSubbagianByJenis(par,function(err2,values2){
            let obj = new Object
            obj = item
            obj.items = values2

            resolve(obj)
          })
        })
      })

      Promise.all(promises)
      .then(hasil=>{
        response.ok(hasil, res);      
      })
      .catch(err=>{
        response.ok(null,err)
      })
    }

    else{
      response.ok([], res);    
    }
    
  })
}

exports.getSoalByJenis = function(req, res) {
  
  Pmb.getSubbagianByJenis(
    req.query, 
    function(err, values) {    
    if (err)
      res.send(err);

    let promises = values.map(function(item){
      return new Promise((resolve,reject)=>{
        let par = new Object
        par.id = item.id
        par.quiz_id = req.query.quiz_id
        Pmb.getSoalByJenis(par,function(err2,values2){
          let obj = new Object
          obj = item
          obj.items = values2

          resolve(obj)
        })
      })
    })

    Promise.all(promises)
    .then(hasil=>{
      response.ok(hasil, res);      
    })
    .catch(err=>{
      response.ok(null,err)
    })
    // response.ok(values, res);
  });
};
