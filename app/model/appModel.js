'user strict';
var sql = require('../../db.js');
// const cryptLib = require('@skavinvarnan/cryptlib');
var unique = require("array-unique").immutable;
// var async = require('async');
// var await = require('await');
var Promise = require('promise');
//Task object constructor
var Pmb = function(task){
   
};

function getMabaDiterima(dataQuery, callback){
    let p = new Promise((resolve, reject) => {
        let tahun = dataQuery.tahun
        let txt = "SELECT * FROM periode WHERE tahun = ? ORDER BY tahun_semester ASC "
        sql.query(txt,[tahun], function(err, res){
            if(err)
                reject(err)
            else{
                resolve(res)
            }
        })
    })
    .then(res1 => {
        let sd = res1[0].tanggal_buka
        let ed = res1[res1.length-1].tanggal_tutup
        if(dataQuery.kode_prodi){
            let params = []
            var txt = "SELECT p.nama, p.tempat_lahir, p.tanggal_lahir, p.jk, p.status_warga_negara "
            txt += " FROM erp_peserta p "
            txt += " JOIN m_kampus k ON k.kode_siakad = p.kampus_id "
            txt += " WHERE 1 "
            txt += " AND (p.prodi_diterima = '"+dataQuery.kode_prodi+"') "
            txt += " AND p.created_at BETWEEN '"+sd+"' and '"+ed+"' "
            txt += " GROUP BY p.nama, p.tempat_lahir, p.tanggal_lahir, p.jk, p.status_warga_negara "
            txt += " ORDER BY p.nama; "
            sql.query(txt,params,function(err, res){
                if(err){
                    console.log(err)
                    callback(err,null);
                }
                else{
                    let hasil = {
                        'count_received' : res.length,
                        'items':res
                    }
                    callback(null, hasil);
                }
            });
        }

        else{
            callback(null, 'Oops, data peserta PMB not found');
        }
    })
    .catch(err=>{
        console.log(err)
        callback(err,null)
    })
    
        
}

function getMabaPendaftar(dataQuery, callback){

    if(dataQuery.tahun && dataQuery.kode_prodi){
        let p = new Promise((resolve, reject) => {
            let tahun = dataQuery.tahun
            let txt = "SELECT * FROM periode WHERE tahun = ? ORDER BY tahun_semester ASC "
            sql.query(txt,[tahun], function(err, res){
                if(err)
                    reject(err)
                else{
                    resolve(res)
                }
            })
        })
        .then(res1 => {
            if(res1[0]){
                let sd = res1[0].tanggal_buka
                let ed = res1[res1.length-1].tanggal_tutup
                
                let params = []
                var txt = "SELECT p.nama, p.tempat_lahir, p.tanggal_lahir, p.jk, p.status_warga_negara "
                txt += " FROM erp_peserta p "
                txt += " WHERE 1 "
                txt += " AND (p.prodi_tujuan1 = '"+dataQuery.kode_prodi+"' OR p.prodi_tujuan2 = '"+dataQuery.kode_prodi+"' OR p.prodi_tujuan3 = '"+dataQuery.kode_prodi+"') "
                txt += " AND p.created_at BETWEEN '"+sd+"' and '"+ed+"' "
                txt += " GROUP BY p.nama, p.tempat_lahir, p.tanggal_lahir, p.jk, p.status_warga_negara "
                txt += " ORDER BY p.nama; "
                sql.query(txt,params,function(err, res){
                    if(err){
                        console.error(err)
                        callback(err,null);
                    }
                    else{
                        let hasil = {
                            'count_received' : res.length,
                            'items':res
                        }
                        callback(null, hasil);
                    }
                });
                
            }

            else{
                callback(null, 'Oops, data Periode Admisi yang aktif tidak ditemukan');
            }
        })
        .catch(err=>{
            console.error(err)
            callback(err,null)
        })
    }

    else{
        callback(null, 'Oops, parameter yang dikirimkan tidak cukup (tahun, kode_prodi)');
    }

    
    
        
}

function getMabaPerprodi(dataQuery, callback){
    let p = new Promise((resolve, reject) => {
        let txt = "SELECT * FROM periode WHERE status_aktivasi = 'Y' ORDER BY tahun_semester ASC "
        sql.query(txt,[], function(err, res){
            if(err)
                reject(err)
            else{
                resolve(res)
            }
        })
    })
    .then(res => {
        let sub = new Promise((resolve, reject) => {
            let tahun = res[0].tahun
            let txt = "SELECT * FROM periode WHERE tahun = ? ORDER BY tahun_semester ASC "
            sql.query(txt,[tahun], function(err, res){
                if(err)
                    reject(err)
                else{
                    resolve(res)
                }
            })
        })
        .then(res1 => {

            let sd = res1[0].tanggal_buka
            let ed = res1[res1.length-1].tanggal_tutup
            if(dataQuery.kode_prodi && dataQuery.kode_kampus){
                let params = [dataQuery.kode_prodi, dataQuery.kode_kampus]
                var txt = "SELECT p.id, p.nama, p.nik, p.nisn, p.va_code, p.is_kelas_internasional, p.prodi_diterima "
                txt += ", p.lama_rencana_kuliah as lama, p.kampus_id, SUM(tr.nominal) as terbayar, "
                txt += " SUM(t.nominal_tagihan) as nominal_tagihan, SUM(t.nominal_minimal) as nominal_minimal "
                txt += ", tm.nama_tagihan as nama_tagihan "
                txt += " FROM erp_peserta p "
                txt += " JOIN m_kampus k ON k.kode_siakad = p.kampus_id "
                txt += " LEFT JOIN erp_tagihan t ON t.peserta_id = p.id "
                txt += " LEFT JOIN erp_tagihan_master tm ON t.tagihan_master_id = tm.id "
                txt += " LEFT JOIN erp_tagihan_rincian tr ON tr.tagihan_id = t.id "
                txt += " WHERE p.prodi_diterima = ? "
                txt += " AND k.kode_siakad = ? "
                txt += " AND p.created_at BETWEEN '"+sd+"' and '"+ed+"' "
                txt += " GROUP BY p.id, tm.nama_tagihan "
                txt += " ORDER BY p.nama; "
                sql.query(txt,params,function(err, res){
                    if(err){
                        console.error(err)
                        callback(err,null);
                    }
                    else{
                        let hasil = {
                            'count_received' : res.length,
                            'items':res
                        }
                        callback(null, hasil);
                    }
                });
            }

            else{
                callback(null, 'Oops, data peserta PMB not found');
            }
        })
    })
    .catch(err=>{
        callback(err,null)
    })
    
        
}

function getTagihanMaba(dataQuery, callback){
    if(dataQuery.pmb_id){
        let params = [dataQuery.pmb_id]
        var txt = "select s.nama as namasub, p.id, p.nama, p.nomor_soal, p.jawaban1, "
        txt += " p.jawaban2, p.jawaban3, p.jawaban4, p.jawaban_benar, "
        txt += " p.poin, p.foto_path from erp_pertanyaan p "
        txt += " JOIN erp_subbagian s ON s.id = p.subbagian_id "
        txt += " WHERE p.subbagian_id = ? AND p.quiz_id = ? ORDER BY nomor_soal; "
        sql.query(txt,params,function(err, res){
            if(err){
                console.log(err)
                callback(err,null);
            }
            else{
                callback(null, res);
            }
        });
    }

    else{
        callback(null, 'Oops, data PMB not found');
    }
        
}


function getCamabaPerhari(callback){
    let p = new Promise((resolve, reject) => {
        let txt = "SELECT * FROM periode WHERE status_aktivasi = 'Y' ORDER BY tahun_semester ASC "
        sql.query(txt,[], function(err, res){
            if(err)
                reject(err)
            else{
                resolve(res)
            }
        })
    })
    .then(res => {
        let sub = new Promise((resolve, reject) => {
            let tahun = res[0].tahun
            let txt = "SELECT * FROM periode WHERE tahun = ? ORDER BY tahun_semester ASC "
            sql.query(txt,[tahun], function(err, res){
                if(err)
                    reject(err)
                else{
                    resolve(res)
                }
            })
        })
        .then(res1 => {

            let sd = res1[0].tanggal_buka
            let ed = res1[res1.length-1].tanggal_tutup
            // console.log(sd+' '+ed)
          
            let txt = "select count(*) as total, date(created_at) as tgl from erp_peserta p where p.is_deleted = '0' AND p.jenjang IS NOT NULL AND p.created_at BETWEEN '"+sd+"' and '"+ed+"' group by date(created_at) order by date(created_at)"

            sql.query(txt,[],function(err, res){
                if(err)
                    callback(err, null)
                else{
                    callback(null, res)
                }
            })    
        })
    })
    .catch(err=>{
        callback(err,null)
    })
    
}

function getRekapCamaba(callback){
    let p = new Promise((resolve, reject) => {
        let txt = "SELECT * FROM periode WHERE status_aktivasi = 'Y' ORDER BY tahun_semester ASC "
        sql.query(txt,[], function(err, res){
            if(err)
                reject(err)
            else{
                resolve(res)
            }
        })
    })
    .then(res => {
        let sub = new Promise((resolve, reject) => {
            let tahun = res[0].tahun
            let txt = "SELECT * FROM periode WHERE tahun = ? ORDER BY tahun_semester ASC "
            sql.query(txt,[tahun], function(err, res){
                if(err)
                    reject(err)
                else{
                    resolve(res)
                }
            })
        })
        .then(res1 => {

            let sd = res1[0].tanggal_buka
            let ed = res1[res1.length-1].tanggal_tutup
            // console.log(sd+' '+ed)
            // let txt = "SELECT (select count(*)  from erp_peserta p where p.jenjang IS NOT NULL AND p.JK = 'L' AND p.created_at BETWEEN '"+sd+"' and '"+ed+"') as L, "
            // txt += "(select count(*)  from erp_peserta p where p.jenjang IS NOT NULL AND p.JK = 'P' AND p.created_at BETWEEN '"+sd+"' and '"+ed+"') as P, "
            // txt += "(select count(*)  from erp_peserta p where p.jenjang IN ('C','D') AND p.created_at BETWEEN '"+sd+"' and '"+ed+"') as sarjana, "
            // txt += "(select count(*)  from erp_peserta p where p.jenjang = 'B' AND p.created_at BETWEEN '"+sd+"' and '"+ed+"') as magister, "
            // txt += "(select count(*)  from erp_peserta p where p.jenjang = 'A' AND p.created_at BETWEEN '"+sd+"' and '"+ed+"') as doktoral, "
            // txt += "(select count(*)  from erp_peserta p where p.jenjang IS NOT NULL AND p.is_kmi = 1 AND p.created_at BETWEEN '"+sd+"' and '"+ed+"') as kmi, "
            // txt += "(select count(*)  from erp_peserta p where p.jenjang IS NOT NULL AND p.is_kmi <> 1 AND p.created_at BETWEEN '"+sd+"' and '"+ed+"') as non_kmi, "
            // txt += "(select count(*)  from erp_peserta p where p.jenjang IS NOT NULL AND p.created_at BETWEEN '"+sd+"' and '"+ed+"') as total "

            let txt = "SELECT "
            
            txt += "(SELECT COUNT(*) FROM (select count(*) from erp_peserta p where p.is_deleted = '0' AND p.jenjang IS NOT NULL AND p.JK = 'L' AND p.created_at BETWEEN '"+sd+"' and '"+ed+"' group by nama, tanggal_lahir, nik, nama_ibu_kandung) as ll) as L, "
            txt += "(SELECT COUNT(*) FROM (select count(*)  from erp_peserta p where p.is_deleted = '0' AND p.jenjang IS NOT NULL AND p.JK = 'P' AND p.created_at BETWEEN '"+sd+"' and '"+ed+"' group by nama, tanggal_lahir, nik, nama_ibu_kandung) as pp) as P, "

            txt += "(SELECT COUNT(*) FROM (select count(*)  from erp_peserta p where p.is_deleted = '0' AND p.jenjang IN ('C','D') AND p.created_at BETWEEN '"+sd+"' and '"+ed+"' group by nama, tanggal_lahir, nik, nama_ibu_kandung) as ss) as sarjana, "
            txt += "(SELECT COUNT(*) FROM (select count(*)  from erp_peserta p where p.is_deleted = '0' AND p.jenjang = 'B' AND p.created_at BETWEEN '"+sd+"' and '"+ed+"' group by nama, tanggal_lahir, nik, nama_ibu_kandung) as mm) as magister, "
            txt += "(SELECT COUNT(*) FROM (select count(*)  from erp_peserta p where p.is_deleted = '0' AND p.jenjang = 'A' AND p.created_at BETWEEN '"+sd+"' and '"+ed+"' group by nama, tanggal_lahir, nik, nama_ibu_kandung) as dd) as doktoral, "

            txt += "(SELECT COUNT(*) FROM (select count(*)  from erp_peserta p where p.is_deleted = '0' "
            txt += " AND p.jenjang IN ('C','D') AND p.tahun_lulus = '"+res1[0].tahun+"' AND p.is_kmi = 1 "
            txt += " AND p.JK = 'L' AND p.created_at BETWEEN '"+sd+"' and '"+ed+"' "
            txt += " group by nama, tanggal_lahir, nik, nama_ibu_kandung) as kk) as kmi_baru_putra, "
            
            txt += "(SELECT COUNT(*) FROM (select count(*)  from erp_peserta p where p.is_deleted = '0' "
            txt += " AND p.jenjang IN ('C','D') AND p.tahun_lulus = '"+res1[0].tahun+"' AND p.is_kmi = 1 "
            txt += " AND p.JK = 'P' AND p.created_at BETWEEN '"+sd+"' and '"+ed+"' "
            txt += " group by nama, tanggal_lahir, nik, nama_ibu_kandung) as kk) as kmi_baru_putri, "

            txt += "(SELECT COUNT(*) FROM (select count(*)  from erp_peserta p where p.is_deleted = '0' "
            txt += " AND p.jenjang IN ('C','D') AND p.tahun_lulus <> '"+res1[0].tahun+"' AND p.is_kmi = 1 "
            txt += " AND p.JK = 'L' AND p.created_at BETWEEN '"+sd+"' and '"+ed+"' "
            txt += " group by nama, tanggal_lahir, nik, nama_ibu_kandung) as kk) as kmi_lama_putra, "
            
            txt += "(SELECT COUNT(*) FROM (select count(*)  from erp_peserta p where p.is_deleted = '0' "
            txt += " AND p.jenjang IN ('C','D') AND p.tahun_lulus <> '"+res1[0].tahun+"' AND p.is_kmi = 1 "
            txt += " AND p.JK = 'P' AND p.created_at BETWEEN '"+sd+"' and '"+ed+"' "
            txt += " group by nama, tanggal_lahir, nik, nama_ibu_kandung) as kk) as kmi_lama_putri, "

            txt += "(SELECT COUNT(*) FROM (select count(*)  from erp_peserta p where p.is_deleted = '0' "
            txt += " AND p.jenjang IN ('C','D') AND p.tahun_lulus IS NULL AND p.created_at "
            txt += " BETWEEN '"+sd+"' and '"+ed+"' group by nama, tanggal_lahir, nik, nama_ibu_kandung) as kk) as tidak_mengisi_kolom_tahun_lulus, "

            txt += "(SELECT COUNT(*) FROM (select count(*)  from erp_peserta p where p.is_deleted = '0' AND "
            txt += " p.jenjang IN ('C','D') AND p.is_kmi <> 1 AND p.JK = 'L' AND p.created_at BETWEEN '"+sd+"' and '"+ed+"' "
            txt += " group by nama, tanggal_lahir, nik, nama_ibu_kandung) as kk) as non_kmi_putra, "
            txt += "(SELECT COUNT(*) FROM (select count(*)  from erp_peserta p where p.is_deleted = '0' AND "
            txt += " p.jenjang IN ('C','D') AND p.is_kmi <> 1 AND p.JK = 'P' AND p.created_at BETWEEN '"+sd+"' and '"+ed+"' "
            txt += " group by nama, tanggal_lahir, nik, nama_ibu_kandung) as kk) as non_kmi_putri, "

            txt += "(SELECT COUNT(*) FROM (select count(*)  from erp_peserta p where p.is_deleted = '0' AND p.jenjang IN ('C','D') AND p.lama_rencana_kuliah = 4 AND p.created_at BETWEEN '"+sd+"' and '"+ed+"' group by nama, tanggal_lahir, nik, nama_ibu_kandung) as ss) as sarjana_4_tahun, "
            txt += "(SELECT COUNT(*) FROM (select count(*)  from erp_peserta p where p.is_deleted = '0' AND p.jenjang IN ('C','D') AND p.lama_rencana_kuliah <> 4 AND p.created_at BETWEEN '"+sd+"' and '"+ed+"' group by nama, tanggal_lahir, nik, nama_ibu_kandung) as ss) as sarjana_1_tahun, "

            txt += "(SELECT COUNT(*) FROM (select count(*)  from erp_peserta p where p.is_deleted = '0' AND p.jenjang IS NOT NULL AND p.created_at BETWEEN '"+sd+"' and '"+ed+"' group by nama, tanggal_lahir, nik, nama_ibu_kandung) as tt) as total "

            sql.query(txt,[],function(err, res){
                if(err)
                    callback(err, null)
                else{
                    hasil = []
                    hasil.push(res[0])
                    hasil.push({'periode' : res1})
                    callback(null, hasil)
                }
            })    
        })
    })
    .catch(err=>{
        callback(err,null)
    })
    
}

function syncJawaban(body, callback){
    cekJawaban(body, function(err, res){

        if(res > 0){
            updateJawaban(body, function(err2, res2){
                if(err2) callback(err2,null)

                callback(null,res2) 
            })
        }

        else{
            insertJawaban(body, function(err2, res2){
                if(err2) callback(err2,null)

                callback(null,res2) 
            })
        }
    })
}

function cekJawaban(body, callback){
    let txt = "SELECT COUNT(*) as total FROM erp_jawaban WHERE peserta_id = ? AND pertanyaan_id = ?;"
    sql.query(txt,[body.peserta_id, body.pertanyaan_id],function(err,res){
        if(err) callback(err,null)

        
        if(res[0].total > 0)
            callback(null,res[0].total)
        else
            callback(null,0)
    })
}

function updateJawaban(body, callback){
    let txt = "UPDATE erp_jawaban SET jawaban = ?, poin = ? WHERE peserta_id = ? AND pertanyaan_id = ?;"
    sql.query(txt,[body.jawaban, body.poin, body.peserta_id, body.pertanyaan_id],function(err,res){
        if(err) callback(err,null)

        callback(null,res)
    })
}

function insertJawaban(body, callback){
    let txt = "INSERT INTO erp_jawaban (peserta_id, pertanyaan_id, jawaban, poin) VALUES(?,?,?,?);"
    sql.query(txt,[body.peserta_id, body.pertanyaan_id, body.jawaban, body.poin],function(err,res){
        if(err) {
            console.log(err)
            callback(err,null)
        }

        callback(null,res)
    })
}

function getJenisSoalProdi(id1, id2, id3, callback){
    var txt = "select pp.jenis_pertanyaan_id as jpid, jp.nama from erp_pertanyaan_prodi pp "
    txt += " JOIN erp_jenis_pertanyaan jp ON jp.id = pp.jenis_pertanyaan_id"
    txt += " WHERE pp.prodi_id IN (?,?,?) GROUP BY pp.jenis_pertanyaan_id ORDER BY jp.nama; "
    sql.query(txt,[id1,id2,id3],function(err, res){
        if(err)
            callback(err,null);
        else{
            callback(null, res);
        }
    });    
}

function getSubbagianByJenis(dataQuery, callback){
    let params = [dataQuery.id, dataQuery.quiz_id]
    var txt = "select s.id, s.nama, COUNT(*) as jml from erp_subbagian s "
    txt += " JOIN erp_pertanyaan p ON p.subbagian_id = s.id "
    txt += " WHERE s.jenis_id = ? AND p.quiz_id = ? GROUP BY s.id, s.nama; "
    sql.query(txt,params,function(err, res){
        if(err){
            callback(err,null);
        }
        else{
            callback(null, res);
        }
    });    
}

function getSoalByJenis(dataQuery, callback){
    let params = [dataQuery.id,dataQuery.quiz_id]
    var txt = "select s.nama as namasub, p.id, p.nama, p.nomor_soal, p.jawaban1, "
    txt += " p.jawaban2, p.jawaban3, p.jawaban4, p.jawaban_benar, "
    txt += " p.poin, p.foto_path from erp_pertanyaan p "
    txt += " JOIN erp_subbagian s ON s.id = p.subbagian_id "
    txt += " WHERE p.subbagian_id = ? AND p.quiz_id = ? ORDER BY nomor_soal; "
    sql.query(txt,params,function(err, res){
        if(err){
            console.log(err)
            callback(err,null);
        }
        else{
            callback(null, res);
        }
    });    
}

function getJawaban(qid, pid, callback){
    var txt = "select peserta_id, pertanyaan_id, jawaban "
    txt += " from erp_jawaban "
    txt += " WHERE pertanyaan_id = ? AND peserta_id = ?; "
    sql.query(txt,[qid, pid],function(err, res){
        if(err){
            console.log(err)
            callback(err,null);
        }
        else{
            callback(null, res);
        }
    });    
}

Pmb.getJawaban = getJawaban
Pmb.syncJawaban = syncJawaban
Pmb.getJenisSoalProdi = getJenisSoalProdi
Pmb.getSoalByJenis = getSoalByJenis
Pmb.getSubbagianByJenis = getSubbagianByJenis
Pmb.getRekapCamaba = getRekapCamaba
Pmb.getCamabaPerhari = getCamabaPerhari
Pmb.getTagihanMaba = getTagihanMaba
Pmb.getMabaPerprodi = getMabaPerprodi
Pmb.getMabaPendaftar = getMabaPendaftar
Pmb.getMabaDiterima = getMabaDiterima
module.exports= Pmb