'user strict';
var sql = require('../../db.js');
const superagent = require('superagent');
var unique = require("array-unique").immutable;
var async = require('async');
var jwt = require('jsonwebtoken');
var moment = require('moment-timezone');
// var async = require('async');
var await = require('await');
var Promise = require('promise');


//Task object constructor
var Oppal = function(task){
   
};

function cekVa(dataPost, callback){
    let Account = dataPost.Account

    let main = new Promise((resolve, reject)=>{
        var txt = "SELECT COUNT(*) as total FROM erp_peserta p ";
        txt += "WHERE p.va_code = ? ";

        sql.query(txt,[Account],function(err, res){
            if(err) {
                console.log(err)
                reject(err);
            }
            else{
                if(res[0])
                    resolve(res[0].total);
                else
                    resolve(0)
            }
        });
    })

    main.then(res => {
        callback(null, res)    
          
    }).catch(err=>{
        console.log(err)
        callback(err,null)
    })
}

function insertTransaksi(dataPost){
    return new Promise((resolve, reject)=>{
        let dt = moment.tz(dataPost.ModifiedDate, 'Asia/Jakarta').local().format('YYYY-MM-DD HH:mm:ss')

        var txt = "INSERT INTO erp_transaksi (TransactionID, Reference, Account, AccountHolder, AmountCurrency,"
        txt += "Amount, AdminFeeCurrency, AdminFee, UniqCodeCurrency, UniqCode, TotalCurrency, Total, Status, ModifiedDate) VALUES "
        txt += " (?,?,?,?,?,?,?,?,?,?,?,?,?,?) "
        sql.query(txt,[
                dataPost.TransactionID, 
                dataPost.Reference, 
                dataPost.Account, 
                dataPost.AccountHolder, 
                dataPost.AmountCurrency, 
                dataPost.Amount,
                dataPost.AdminFeeCurrency,
                dataPost.AdminFee,
                dataPost.UniqCodeCurrency,
                dataPost.UniqCode,
                dataPost.TotalCurrency,
                dataPost.Total,
                dataPost.Status,
                dt,
            ],function(err, res){
            if(err) {
                console.log(err)
                if(err.errno == 1062)
                    resolve("Maaf, TransactionID sudah dipakai")  
                else
                    reject(err)

            }
            else
                resolve(res);
        });
    })
}

function oppal_pay(dataPost, callback){
    let params = []
    let dt = moment.tz(dataPost.ModifiedDate, 'Asia/Jakarta').local().format('YYYY-MM-DD HH:mm:ss')
    let TransactionID = dataPost.TransactionID
    let Reference = dataPost.Reference
    let Account = dataPost.Account
    let AccountHolder = dataPost.AccountHolder
    let AmountCurrency = dataPost.AmountCurrency
    let Amount = dataPost.Amount
    let AdminFeeCurrency = dataPost.AdminFeeCurrency
    let AdminFee = dataPost.AdminFee
    let UniqCodeCurrency = dataPost.UniqCodeCurrency
    let UniqCode = dataPost.UniqCode
    let TotalCurrency = dataPost.TotalCurrency
    let Total = dataPost.Total
    let Status = dataPost.Status
    let ModifiedDate = dt

    if(!TransactionID){
    	callback('Invalid params', 'TransactionID is empty')
    }

    if(!ModifiedDate){
    	callback('Invalid params', 'ModifiedDate is empty')
    }

    let resp = {
        "Status" : "success"
    }

    let trx = insertTransaksi(dataPost)
    trx.then(res=>{
        let main = new Promise((resolve, reject)=>{
            var txt = "SELECT t.id FROM erp_tagihan t ";
            txt += "JOIN erp_peserta p ON p.id = t.peserta_id "
            txt += "WHERE p.va_code = ? LIMIT 1 ";

            sql.query(txt,[Account],function(err, res){
                if(err) {
                    console.log(err)
                    reject(err);
                }
                else{
                    if(res[0])
                        resolve(res[0]);
                    else
                        resolve({"Status":"tagihannotfound"})
                }
            });
        })

        main.then(res => {
            if(res.id){
                var txt = "INSERT INTO erp_tagihan_rincian (no_trx, tagihan_id, nominal, tanggal_trx, no_reff,status) VALUES "
                txt += " (?,?,?,?,?,?) "
                sql.query(txt,[TransactionID, res.id, Amount, ModifiedDate, Reference, Status],function(err, hsl){
                    if(err) {
                        // console.log(err)
                        if(err.errno == 1062)
                            callback(null, "Maaf, TransactionID sudah dipakai")  
                        else
                            callback(null, err.sqlMessage)

                    }
                    else {
                        // Update nominal_terbayar in erp_tagihan

                        updateTagihanNominalTerbayar(res.id, function(updateErr, updateResp) {
                            if (updateErr) {
                                callback(null, updateErr);
                            } else {
                                callback(null, resp);
                            }
                        });
                    }
                });
            }
            else{
                callback(null, res)    
            }
              
        }).catch(err=>{
            console.log(err)
            callback(err,null)
        })
    }).catch(err=>{
        console.log(err)
        callback(err,null)
    })
}

// Update nominal_terbayar in erp_tagihan based on the sum of erp_tagihan_rincian.nominal
function updateTagihanNominalTerbayar(tagihanId, callback) {
    const updateQuery = `
        UPDATE erp_tagihan t
        JOIN (
            SELECT tagihan_id, SUM(nominal) AS total_nominal
            FROM erp_tagihan_rincian
            GROUP BY tagihan_id
        ) r ON t.id = r.tagihan_id
        SET t.nominal_terbayar = r.total_nominal
        WHERE t.id = ?`;

    sql.query(updateQuery, [tagihanId], function(err, res) {
        if (err) {
            console.error("Error updating nominal_terbayar:", err);
            callback(err, null);
        } else {
            console.log(tagihanId)
            callback(null, "nominal_terbayar updated successfully");
        }
    });
}

Oppal.cekVa = cekVa
Oppal.oppal_pay = oppal_pay
module.exports= Oppal;